from config import *
from datetime import datetime, timedelta


@app.route('/users/deposits', methods=['GET'])
@require_key
def transaction():
    token = get_token()
    agent_id = request.args['agent_id']
    username = request.args['username']
    _from = 0
    _to = 0
    try:
        if request.args['from'] and request.args['to']:
            _from = date_convert(request.args['from'])
            _to = date_convert(request.args['to'])
        else:
            _from = date_convert(str(datetime.today().date() - timedelta(days=1)))
            _to = date_convert(str(datetime.today().date()))

        url_string = f"https://123bet.api-hub.com/spa/cash/list/manual"

        header = {
            'Content-Type': 'application/json',
            'Authorization': f'Bearer {token}',
            'Accept-Encoding': 'gzip, deflate',
            'Host': '123bet.api-hub.com'
        }

        params = {
            "dateFrom": _from,
            "dateTo": _to,
            "member": username
        }
        res = requests.get(url_string, headers=header, params=params, verify=False)
        return res.json()

    except Exception as e:
        return jsonify({
            'status': 'Error',
            'message': str(e)
        }), 400


def date_convert(date_str):
    date_obj = datetime.strptime(date_str, "%Y-%m-%d")
    return date_obj.strftime("%d/%m/%Y")
