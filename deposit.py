from config import *
import time


@app.route('/deposit', methods=['POST'])
@require_key
def deposit():
    username = request.json['username']
    value = request.json['value']
    ref = str(round(time.time(), 3)).replace(".", "")
    user_id, token = get_user_id(username)

    header = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {token}',
        'Accept-Encoding': 'gzip, deflate',
        'Host': '123bet.api-hub.com'
    }

    payload = {
        "memberId": user_id,
        "amount": value,
        "ref": ref,
        "modifyType": "ADD"
    }

    url_string = "https://123bet.api-hub.com/spa/cash/credit/modify_manual"

    try:
        res = requests.post(url_string, headers=header, json=payload, verify=False)
        if res.json()['message'] == 'success':
            return jsonify({
                'status': 'OK',
                'result': {
                    'username': username,
                    'amount': value
                }
            })

        else:
            return res.json()

    except Exception as e:
        return jsonify({
            'status': 'Error',
            'message': str(e)
        })
