from config import *


@app.route('/users/suspend', methods=['POST'])
@require_key
def suspend():
    username = request.json['username']
    _suspend = request.json['suspend']
    lock = request.json['lock']
    status = request.json['status']

    user_id, token = get_user_id(username)

    header = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {token}',
        'Accept-Encoding': 'gzip, deflate',
        'Host': '123bet.api-hub.com'
    }

    payload = {
        "suspend": bool(_suspend),
        "lock": bool(lock),
        "active": bool(status)
    }

    url_string = f"https://123bet.api-hub.com/spa/member/status/{user_id}"
    try:
        res = requests.put(url_string, headers=header, json=payload, verify=False)
        if res.json()['message'] == 'success':
            return jsonify({
                'status': 'OK',
                'result': {
                    'username': username,
                    "suspend": bool(_suspend),
                    "lock": bool(lock),
                    "active": bool(status)
                }
            })

        else:
            return res.json()
    except Exception as e:
        return jsonify({
            'status': 'Error',
            'message': str(e)
        })
