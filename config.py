import os
from dotenv import load_dotenv
from flask import Flask, request, jsonify, abort
from flask_cors import CORS
from functools import wraps
import requests

load_dotenv()

app = Flask(__name__)
CORS(app, resources={r"/*": {"origins": "*"}})


def get_token():
    cred = {
        "username": os.getenv('123_USER'),
        "password": os.getenv('123_PASSWORD')
    }

    res = requests.post('https://sportbook-123bet.serverless-hub.com/spa/user/login', json=cred)

    return res.json()['result']['access_token']


# Return user_id and access token
def get_user_id(username):
    token = get_token()
    header = {
        'Authorization': f"Bearer {token}"
    }

    url_string = f"https://123bet.api-hub.com/spa/member/list?username={username}"
    res = requests.get(url_string, headers=header, verify=False)

    return res.json()["result"].get("list")[0]['_id'], token


def require_key(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if request.headers.get('api_key') and request.headers.get('api_key') == os.getenv("API_KEY"):
            return func(*args, **kwargs)
        else:
            abort(401)

    return decorated_function
