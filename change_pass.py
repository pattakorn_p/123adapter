from config import *


@app.route('/users', methods=['PUT'])
@require_key
def change_password():

    username = request.json['username']
    new_password = request.json['password']
    user_id, token = get_user_id(username)

    header = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {token}',
        'Accept-Encoding': 'gzip, deflate',
        'Host': '123bet.api-hub.com'
    }

    payload = """{"commissionSetting":{"casino":{"ag":0,"dg":0,"sa":0,"sexy":0},"game":{"slotXO":0},"multi":{
    "amb":0},"other":{"m2":0},"parlay":{"com":0},"sportsBook":{"hdpOuOe":0,"oneTwoDoubleChance":0,"others":0,
    "typeHdpOuOe":"A"},"step":{"com10":0,"com11":0,"com12":0,"com2":0,"com3":0,"com4":0,"com5":0,"com6":0,"com7":0,
    "com8":0,"com9":0}},"creditLimit":0,"creditModify":false,"limitSetting":{"casino":{"ag":{"isEnable":true,
    "limit":1},"dg":{"isEnable":true,"limit":1},"isWlEnable":false,"sa":{"isEnable":true,"limit":1},
    "sexy":{"isEnable":true,"limit":1},"winPerDay":0},"game":{"slotXO":{"isEnable":true}},"lotto":{"amb":{"_1BOT":{
    "discount":0,"max":1000000,"payout":4.2},"_1TOP":{"discount":0,"max":1000000,"payout":3.2},"_2BOT":{"discount":0,
    "max":20000,"payout":90},"_2BOT_OE":{"discount":3,"max":50000,"payout":1.9},"_2BOT_OU":{"discount":3,"max":50000,
    "payout":1.9},"_2TOD":{"discount":28,"max":20000,"payout":12},"_2TOP":{"discount":0,"max":20000,"payout":90},
    "_2TOP_OE":{"discount":3,"max":50000,"payout":1.9},"_2TOP_OU":{"discount":3,"max":50000,"payout":1.9},
    "_3BOT":{"discount":0,"max":20000,"payout":140},"_3TOD":{"discount":0,"max":20000,"payout":150},
    "_3TOP":{"discount":0,"max":5000,"payout":850},"_3TOP_OE":{"discount":3,"max":50000,"payout":1.9},"_3TOP_OU":{
    "discount":3,"max":50000,"payout":1.9},"_4TOD":{"discount":0,"max":0,"payout":0},"_4TOP":{"discount":0,"max":0,
    "payout":0},"_5TOP":{"discount":0,"max":0,"payout":0},"_6TOP":{"discount":0,"max":0,"payout":0},"hour":15,
    "isEnable":true,"minute":20},"laos":{"hour":20,"isEnable":true,"L_2FB":{"discount":10,"max":3000,"payout":15},
    "L_3TOD":{"discount":10,"max":3000,"payout":25},"L_3TOP":{"discount":10,"max":3000,"payout":350},
    "L_4TOD":{"discount":10,"max":3000,"payout":40},"L_4TOP":{"discount":10,"max":3000,"payout":1000},"minute":0,
    "T_1BOT":{"discount":0,"max":1000000,"payout":4.2},"T_1TOP":{"discount":0,"max":1000000,"payout":3.2},
    "T_2BOT":{"discount":0,"max":20000,"payout":90},"T_2TOD":{"discount":0,"max":20000,"payout":12},"T_2TOP":{
    "discount":0,"max":20000,"payout":90},"T_3TOD":{"discount":0,"max":20000,"payout":150},"T_3TOP":{"discount":0,
    "max":5000,"payout":850},"T_4TOD":{"discount":0,"max":0,"payout":0},"T_4TOP":{"discount":0,"max":0,"payout":0}},
    "pp":{"_1BOT":{"discount":0,"max":100000,"payout":4},"_1TOP":{"discount":0,"max":100000,"payout":3},
    "_2BOT":{"discount":0,"max":10000,"payout":90},"_2BOT_OE":{"discount":0,"max":0,"payout":0},"_2BOT_OU":{
    "discount":0,"max":0,"payout":0},"_2TOD":{"discount":0,"max":100000,"payout":12},"_2TOP":{"discount":0,
    "max":10000,"payout":90},"_2TOP_OE":{"discount":0,"max":0,"payout":0},"_2TOP_OU":{"discount":0,"max":0,
    "payout":0},"_3BOT":{"discount":0,"max":0,"payout":0},"_3TOD":{"discount":0,"max":10000,"payout":130},
    "_3TOP":{"discount":0,"max":2000,"payout":800},"_3TOP_OE":{"discount":0,"max":0,"payout":0},"_3TOP_OU":{
    "discount":0,"max":0,"payout":0},"_4TOD":{"discount":0,"max":10000,"payout":225},"_4TOP":{"discount":0,
    "max":1000,"payout":5000},"_5TOP":{"discount":0,"max":1000,"payout":25000},"_6TOP":{"discount":0,"max":1000,
    "payout":50000},"hour":0,"isEnable":true,"minute":0}},"multi":{"amb":{"isEnable":true}},"other":{"m2":{
    "isEnable":true,"maxBetPerDay":100000,"maxBetPerMatchHDP":50000,"maxMatchPerBet":12,"maxPayPerBill":100000,
    "maxPerBet":5000,"maxPerBetHDP":10000,"minMatchPerBet":2,"minPerBet":20,"minPerBetHDP":20}},"sportsBook":{
    "hdpOuOe":{"maxPerBet":100000,"maxPerMatch":100000},"isEnable":true,"oneTwoDoubleChance":{"maxPerBet":100000},
    "others":{"maxPerBet":100000},"outright":{"maxPerMatch":100000}},"step":{"parlay":{"isEnable":true,
    "maxBetPerDay":1000000,"maxMatchPerBet":12,"maxPayPerBill":1000000,"maxPerBet":10000,"minMatchPerBet":2,
    "minPerBet":50},"step":{"isEnable":true,"maxBetPerDay":1000000,"maxMatchPerBet":12,"maxPayPerBill":1000000,
    "maxPerBet":10000,"minMatchPerBet":2,"minPerBet":50}}},"password":"%s","settingEnable":{"ambLottoEnable":true,
    "ambLottoLaosEnable":true,"ambLottoPPEnable":true,"casinoAgEnable":true,"casinoDgEnable":true,
    "casinoSaEnable":true,"casinoSexyEnable":true,"gameCardEnable":true,"gameSlotEnable":true,"otherM2Enable":true,
    "parlayEnable":true,"sportBookEnable":true,"stepEnable":true},"shareSetting":{"casino":{"ag":{"parent":0},
    "dg":{"parent":0},"sa":{"parent":0},"sexy":{"parent":0}},"game":{"slotXO":{"parent":0}},"lotto":{"amb":{
    "parent":0},"laos":{"parent":0},"pp":{"parent":0}},"multi":{"amb":{"parent":0}},"other":{"m2":{"parent":0}},
    "sportsBook":{"live":{"parent":85},"today":{"parent":85}},"step":{"parlay":{"parent":85},
    "step":{"parent":85}}}}""" % new_password

    url_string = f"https://123bet.api-hub.com/spa/member/update/{user_id}"

    try:
        res = requests.put(url_string, headers=header, data=payload, verify=False)
        if res.json()['message'] == 'success':
            return jsonify({
                'status': 'OK',
                'result': {
                    'username': username,
                    'new_password': new_password
                }
            })

        else:
            return res.json()

    except Exception as e:
        return jsonify({
            'status': 'Error',
            'message': str(e)
        })
