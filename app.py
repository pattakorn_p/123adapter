from register import *
from change_pass import *
from suspend import *
from deposit import *
from withdraw import *
from transaction import *


@app.route('/test', methods=['GET'])
def test():
    return "Hello"


if __name__ == "__main__":
    app.run(host='0.0.0.0')
